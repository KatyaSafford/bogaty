from .. import tax
import unittest
import sys

class UnitTests(unittest.TestCase):

	#test based on an income example
	def test_main_example(self):
		result = tax.main(60000, 5000, 5000, 1000, "single")
		self.assertAlmostEqual(result, 8043.75, places=2)

	#test that tax amount increases when income increases
	def test_main_income_increase(self):
		incomes = range(20000, 500000, 10000)
		pairs = zip(incomes[:-1], incomes[1:])
		for pair in pairs:
			self.assertTrue(tax.main(pair[0], 5000, 5000, 1000, "single") < tax.main(pair[1], 5000, 5000, 1000, "single"))

	#test that tax amount decreases when 401k contribution contribution increases
	def test_main_contribution_increase(self):
		contributions = range(1000, 18000, 1000)
		pairs = zip(contributions[:-1], contributions[1:])	
		for pair in pairs:
			self.assertTrue(tax.main(60000, pair[0], 5000, 1000, "single") > tax.main(60000, pair[1], 5000, 1000, "single"))


	def test_get_levels(self):
		status = "single"
		levels = tax.get_levels(status)
		self.assertEqual(levels, [9225, 37450, 90750, 189300, 411500, 413200, sys.maxint])

		status = "head_of_household"
		levels = tax.get_levels(status)
		self.assertEqual(levels, [13150, 50200, 129600, 209850, 411500, 439000, sys.maxint])

		status = "married_jointly"
		levels = tax.get_levels(status)
		self.assertEqual(levels, [18450, 74900, 151200, 230450, 411500, 464850, sys.maxint])

		status = "married_separately"
		levels = tax.get_levels(status)
		self.assertEqual(levels, [9225, 37450, 75600, 115225, 205750, 232425, sys.maxint])

		status = "unrelated"
		self.assertRaises(ValueError, tax.get_levels, status)

	
		

