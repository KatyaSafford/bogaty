# README #

This is a small tax calculator. It calculates tax amount based on income, filling status and some pre-taxed contributions (traditional 401k, traditional IRA and FSA). 

# How to calculate taxes
Main method calculates total income tax. To run this calculation, navigate to project directory in Terminal and type:
```python tax.py <income> <contributed_401k> <contributed_ira> <contributed_fsa> <filing_status>```

Like this: ```python tax.py 80000 10000 3000 0 single```

<filing_status> can be one of the following: ["single", "head_of_household", "married_jointly", "married_separately"]

# How to run unit tests 

To run unit test, first ```pip install nose```.  
Then simply navigate to project root directory in Terminal and type 
```nosetests```