import sys

status_list = ["single", "head_of_household", "married_jointly", "married_separately"]
tax_rates = [0.1, 0.15, 0.25, 0.28, 0.33, 0.35, 0.396]

max_401k = 18000
max_ira = 5500
max_fsa = 2550

#eitc_rates = [14820, 39131, 44454, 47747, 20330, 44651, 49974, 53267]

def check_status(status):
	if status not in status_list:
		raise ValueError("Status must be one of: {}".format(status_list))

#getting tax rates for different income for different filing status
def get_levels(status):
	check_status(status)
	# tax rates for filing single
	if status == status_list[0]: 
		levels = [9225, 37450, 90750, 189300, 411500, 413200, sys.maxint]
	# tax rates for filing as a head of household
	elif status == status_list[1]:
		levels = [13150, 50200, 129600, 209850, 411500, 439000, sys.maxint]
	# tax rates for married filing jointly
	elif status == status_list[2]:
		levels = [18450, 74900, 151200, 230450, 411500, 464850, sys.maxint]
	# tax rates for married filing separately
	elif status == status_list[3]:
		levels = [9225, 37450, 75600, 115225, 205750, 232425, sys.maxint]
	
	return levels

def calc_max_taxable_amount(magi, status):
	magi_levels = get_levels(status)
	max_taxable_amount = 0
	if (magi <= magi_levels[0]):
		max_taxable_amount = magi_levels[0]
	else: 
		for i in range(len(magi_levels)):
			if  (magi < magi_levels[i]):
				max_taxable_amount = magi_levels[i-1]
				break
	#print max_taxable_amount
	return max_taxable_amount

def calc_min_level_taxes(magi, status):
	magi_levels = get_levels(status)

	min_level_tax_amount = 0
	if (magi <= magi_levels[0]):
		min_level_tax_amount = magi * tax_rates[0]
	else:
		min_level_tax_amount = magi_levels[0] * tax_rates[0]
	#print "min level taxes {}".format(min_level_tax_amount)
	return min_level_tax_amount

def calc_medium_level_taxes(magi, status):
	max_taxable_amount = calc_max_taxable_amount(magi, status)
	magi_levels = get_levels(status)
	
	medium_levels_tax_amount = 0
	if (magi < magi_levels[0]):
		print "medium level tax amount{}".format(medium_levels_tax_amount)
		return medium_levels_tax_amount
	else:
		stop_point = magi_levels.index(max_taxable_amount)
		for i in range(1, stop_point+1):
			medium_levels_tax_amount += tax_rates[i] * (magi_levels[i] - magi_levels[i-1]);
			#print "tax amount {}".format(medium_levels_tax_amount)
	#print "medium level tax amount {}".format(medium_levels_tax_amount)
	return medium_levels_tax_amount

def calc_max_level_taxes(magi, status):
	max_taxable_amount = calc_max_taxable_amount(magi, status)
	magi_levels = get_levels(status)

	max_level_tax_amount = 0
	if (magi >= max_taxable_amount):
		stop_point = magi_levels.index(max_taxable_amount)
		#print "stop point {}".format(stop_point)
		max_level_tax_amount = tax_rates[stop_point+1] * (magi - magi_levels[stop_point])
	else:
		max_level_tax_amount

	#print "max level tax amount {}".format(max_level_tax_amount)
	return max_level_tax_amount

def calc_magi(income, traditional_401k_contributed, traditional_ira_contributed, fsa_contributed):
	if ((traditional_401k_contributed <= max_401k) and (traditional_ira_contributed <= max_ira) and (fsa_contributed <= max_fsa)):
		magi = income - traditional_401k_contributed - traditional_ira_contributed - fsa_contributed
		return magi
	else:
		raise ValueError("Pre-taxed contributions can't exceed allowed limits")

#calculate tax amount for given magi based on filing status
def calc_taxes(magi, status):
	tax_amount = calc_min_level_taxes(magi, status) + calc_medium_level_taxes(magi, status) + calc_max_level_taxes(magi, status)
	#print "TOTAL TAX AMOUNT {}".format(tax_amount)
	return tax_amount

def main(income, traditional_401k_contributed, traditional_ira_contributed, fsa_contributed, status):
	pretaxed_amount = traditional_401k_contributed + traditional_ira_contributed + fsa_contributed
	if (income > pretaxed_amount):
		magi = calc_magi(income, traditional_401k_contributed, traditional_ira_contributed, fsa_contributed)
		tax_amount = calc_taxes(magi, status)
		# print "Tax amount to pay: {}".format(tax_amount)
		return tax_amount
	else:
		raise ValueError("Income must be bigger than pretaxed contributions!")

if __name__ == "__main__":
	args = sys.argv
	
	if len(args)!=6:
		raise ValueError("Requires exactly 5 arguments in the following order: income, contrib_401k, contrib_ira, contrib_fsa, status")
		
	income = float(args[1])
	contrib_401k = float(args[2])
	contrib_ira = float(args[3])
	contrib_fsa = float(args[4])
	status = args[5]

	print main(income, contrib_401k, contrib_ira, contrib_fsa, status)








